//= require store/spree_core
//= require ./jquery.eislideshow
//= require ./jquery.easing.1.3


$(function() {
  $('#ei-slider').eislideshow({
    animation     : 'center',
  autoplay      : true,
  slideshow_interval  : 3000,
  titlesFactor    : 0
  });
});
